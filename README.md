# MYOB coding exercise

This is Max Wang's coding exercise.

## Technical Decision

- C#
- .NET Core SDK 3.1
- Visual Studio 2019
- Mediator
- xUnit
- Moq

**Note:**
I use .net core 3.1 instead of 5.0, just because .net core 3.1 is a long-time supported version. The project could be converted to .net core 5.0 if needed. .Net core 6 is very new, but could migrate to .net core 6 if needed.

## Design Overview

- Use the **CQRS** pattern for this simple project with below reasons:
  - **CQRS** allows the read and write workloads to scale independently
  - easy extension for complex business logic
  - business logic can be tested without UI
- Console only talk to **MediatR**, and no complex logic
- **MediatR** handler talk to different services, could be very complex
- Console login will be very easy

## This solution is based on the below assumptions:

- Data validation is based on my assumption.
- Do not need save record.
- Do not need load tax rate from DB, configuration etc. I could add this if needed.
- Do not add the cache, but could add this if needed.
- Output format is simple as asked.
- No log needed. But could add NLog or Serilog with lines of code

## Folder Description

- `src` project source code
- `tests` all tests
- `docs` origin coding exercise document

### src

- `ConsoleApp` main project
- `Application`
- `Infrastructure`
- `Domain`

## run program

You could run program in `bin\Debug` or `bin\Release` folder after you build the project:
`GenerateMonthlyPayslip "Mary Song" 60000`
