﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Employee.Commands.CreateEmployeeMonthlyPayslip
{
    public class CreateEmployeeMonthlyPayslipCommandValidator : AbstractValidator<CreateEmployeeMonthlyPayslipCommand>
    {
        public CreateEmployeeMonthlyPayslipCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("Name is required");

            RuleFor(x => x.AnnualSalary)
                .NotNull().WithMessage("Annual Salary is requried.")
               .GreaterThan(0).WithMessage("Annual Salary must be great than 0.");

        }
    }
}
