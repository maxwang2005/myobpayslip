﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Employee.Commands.CreateEmployeeMonthlyPayslip
{
    public class CreateEmployeeMonthlyPayslipCommand : IRequest<string>
    {
        public string Name { get; set; }

        public decimal AnnualSalary { get; set; }
    }

    public class CreateEmployeeMonthlyPayslipCommandHandler : IRequestHandler<CreateEmployeeMonthlyPayslipCommand, string>
    {
        private readonly IMonthlyPayslip _monthlyPayslipService;
        public CreateEmployeeMonthlyPayslipCommandHandler(IMonthlyPayslip monthlyPayslip)
        {
            _monthlyPayslipService = monthlyPayslip ?? throw new ArgumentNullException(nameof(monthlyPayslip));
        }

        public async Task<string> Handle(CreateEmployeeMonthlyPayslipCommand request, CancellationToken cancellationToken)
        {
            return await _monthlyPayslipService.GenerateMonthlyPayslip(request.Name, request.AnnualSalary);
        }
    }

   


}
