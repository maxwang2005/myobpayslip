﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IMonthlyPayslipOutputFormatter
    {
        public string PayslipFormatter(MonthlyPayslip payslip);
    }
}
