﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ITaxCalculator
    {
        Task<decimal> CalculateAnnualTax(decimal annualSalary);
    }
}
