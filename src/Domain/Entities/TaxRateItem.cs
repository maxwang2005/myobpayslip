﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class TaxRateItem
    {
        public decimal TaxRateMinimumSalary { get; set; }
        public decimal TaxRateMaximumSalary { get; set; }
        public decimal TaxRate { get; set; }
    }
}
