﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Employee
    {
        public string Name { get; set; }
        public decimal AnnualSalary { get; set; }
    }
}
