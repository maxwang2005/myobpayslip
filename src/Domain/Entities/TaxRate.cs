﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    /// <summary>
    ///   Tax Rate details
    /// </summary>
    /// <remarks>
    /// Might need put a date range if rate might be different depend on date
    /// </remarks>
    public class TaxRate
    {
        public string Name { get; set; }
        public IList<TaxRateItem> TaxRates { get; set; }
    }
}
