﻿using System;

namespace Domain.Entities
{
    public class MonthlyPayslip
    {
        public MonthlyPayslip(string name, decimal annualSalay)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException(nameof(name));
            }

            if (annualSalay < 0)
            {
                throw new ArgumentException(nameof(annualSalay));
            }

            Name = name;
            AnnualSalary = annualSalay;
        }

        public decimal AnnualSalary { get; }
        public decimal GrossIncome { get; set; }
        public decimal IncomeTax { get; set; }
        public string Name { get; set; }
        public decimal NetIncome { get; set; }
    }
}