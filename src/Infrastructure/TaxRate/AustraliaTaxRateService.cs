﻿using Application.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.TaxRate
{
    public class AustraliaTaxRateService : ITaxRate
    {
        /// <summary>
        /// Gets the Australia tax rate.
        /// </summary>
        /// put the data in memory for testing purpose only
        /// Could put to configuration file, db, cache etc if needed
        /// <remarks>
        /// </remarks>
        public Domain.Entities.TaxRate GetTaxRate()
        {
            return new Domain.Entities.TaxRate
            {
                Name = "Australia Tax Rate",
                TaxRates = new List<TaxRateItem>
                {
                    new TaxRateItem
                    {
                        TaxRateMinimumSalary = 0,
                        TaxRateMaximumSalary = 20000,
                        TaxRate = 0
                    },
                    new TaxRateItem
                    {
                        TaxRateMinimumSalary = 20001,
                        TaxRateMaximumSalary = 40000,
                        TaxRate = 0.1m
                    },
                    new TaxRateItem
                    {
                        TaxRateMinimumSalary = 40001,
                        TaxRateMaximumSalary = 80000,
                        TaxRate = 0.2m
                    },
                    new TaxRateItem
                    {
                        TaxRateMinimumSalary = 80001,
                        TaxRateMaximumSalary = 180000,
                        TaxRate = 0.3m
                    },
                    new TaxRateItem
                    {
                        TaxRateMinimumSalary = 180001,
                        TaxRateMaximumSalary = decimal.MaxValue,
                        TaxRate = 0.4m
                    },
                }
            };
        }
    }
}
