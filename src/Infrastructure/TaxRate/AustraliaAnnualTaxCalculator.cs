﻿using Application.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.TaxRate
{
    public class AustraliaAnnualTaxCalculator : ITaxCalculator
    {
        private readonly Domain.Entities.TaxRate _taxRate;

        public AustraliaAnnualTaxCalculator(ITaxRate rateServcie)
        {
            if (rateServcie == null)
            {
                throw new ArgumentNullException(nameof(rateServcie));
            }

            _taxRate = rateServcie.GetTaxRate();
        }

        public async Task<decimal> CalculateAnnualTax(decimal annualSalary)
        {
            var currentRate = _taxRate.TaxRates.FirstOrDefault(x => annualSalary >= x.TaxRateMinimumSalary
                && annualSalary <= x.TaxRateMaximumSalary);

            if (currentRate == null)
            {
                throw new ArgumentException("Could not find tax rate, check tax rate settings");
            }

            var taxRateLeft = _taxRate.TaxRates.Where(x => x.TaxRateMaximumSalary < annualSalary);

            return await Task.FromResult((annualSalary
                                          - currentRate.TaxRateMinimumSalary
                                          + 1) * currentRate.TaxRate
                + taxRateLeft.Sum(x => ((x.TaxRateMaximumSalary
                                         - x.TaxRateMinimumSalary
                                         + 1) * x.TaxRate)));
        }
    }
}
