﻿using Application.Interfaces;
using Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Infrastructure.TaxRate
{
    public class GenerateAustraliaMonthlyPayslip : IMonthlyPayslip
    {
        private readonly ITaxCalculator _taxCalculator;
        private readonly IMonthlyPayslipOutputFormatter _formatter;
        public GenerateAustraliaMonthlyPayslip(ITaxCalculator taxCalculator, IMonthlyPayslipOutputFormatter formatter)
        {
            _taxCalculator = taxCalculator ?? throw new ArgumentNullException(nameof(taxCalculator));
            _formatter = formatter ?? throw new ArgumentNullException(nameof(formatter));
        }

        public async Task<string> GenerateMonthlyPayslip(string name, decimal annualSalay)
        {
            var result = new MonthlyPayslip(name, annualSalay);

            result.GrossIncome = annualSalay/12;
            result.IncomeTax = (await _taxCalculator.CalculateAnnualTax(annualSalay)) / 12;
            
            result.NetIncome = result.GrossIncome - result.IncomeTax;

            return await Task.FromResult(_formatter.PayslipFormatter(result));
        }
    }
}