﻿using Application.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Paymentslip
{
    public class ConsoleMonthlyPayslipOutputFormatter : IMonthlyPayslipOutputFormatter
    {
        public string PayslipFormatter(MonthlyPayslip payslip)
        {
            return payslip == null
                ? string.Empty
                : $@"Monthly Payslip for: {payslip.Name}
Gross Monthly Income: {payslip.GrossIncome:C2}
Monthly Income Tax: {payslip.IncomeTax:C2}
Net Monthly Income: {payslip.NetIncome:C2}";
        }
    }
}
