﻿using Application.Interfaces;
using Infrastructure.Paymentslip;
using Infrastructure.TaxRate;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        /// <summary>Inject all infrastructure level services</summary>
        /// <param name="services">The services.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            
            services.AddTransient<ITaxRate, AustraliaTaxRateService>();
            services.AddTransient<ITaxCalculator, AustraliaAnnualTaxCalculator>();
            services.AddTransient<IMonthlyPayslip, GenerateAustraliaMonthlyPayslip>();
            services.AddTransient<IMonthlyPayslipOutputFormatter, ConsoleMonthlyPayslipOutputFormatter>();

            return services;
        }
    }
}
