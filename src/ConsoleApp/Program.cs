﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace GenerateMonthlyPayslip
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var services = Startup.ConfigureServices().BuildServiceProvider();
            await services.GetService<App>().Run(args).ConfigureAwait(false);
        }
    }
}
