﻿using Application.Employee.Commands.CreateEmployeeMonthlyPayslip;
using Application.Exceptions;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateMonthlyPayslip
{
    public class App
    {
        private readonly ISender _mediator = null!;
        public App(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task Run(string[] args)
        {
            if (args == null || args.Length != 2)
            {
                WriteHelpInformation();
                return;
            }

            var name = args[0];
            if (string.IsNullOrWhiteSpace(name))
            {
                WriteHelpInformation();
                return;
            }

            decimal salary = 0;

            if (!decimal.TryParse(args[1], out salary))
            {
                WriteHelpInformation();
                return;
            }


            try
            {
                var payslipOutput = await _mediator.Send(new CreateEmployeeMonthlyPayslipCommand
                {
                    Name = name,
                    AnnualSalary = salary
                });

                Console.WriteLine(payslipOutput);
                
            }
            catch (ValidationException ve)
            {
                WriteInformation(string.Join(";", ve.Errors.SelectMany(x => x.Value)));
            }
            catch (Exception ex)
            {
                WriteInformation(ex.Message);
                //Could add more logger etc to here
            }
            Console.WriteLine("Press any key to exist");
            Console.ReadLine();


        }

        private void WriteHelpInformation()
        {
            WriteInformation("Please use Generate Monthly Payslip with argument as below", ConsoleColor.Yellow);
            WriteInformation("GenerateMonthlyPayslip \"Mary Song\" 60000", ConsoleColor.Yellow);
            WriteInformation("\"Mary Song\" is employee's name, is a required field", ConsoleColor.Yellow);
            WriteInformation("60000 is annual salary, is a required field", ConsoleColor.Yellow);
        }

        private void WriteInformation(string message, ConsoleColor consoleColor = ConsoleColor.White)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
