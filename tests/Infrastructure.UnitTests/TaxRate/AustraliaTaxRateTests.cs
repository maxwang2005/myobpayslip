﻿using Domain.Entities;
using FluentAssertions;
using Infrastructure.TaxRate;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Infrastructure.UnitTests.TaxRate
{
    public class AustraliaTaxRateTests
    {
        /// <summary>
        /// Gets the tax rate should return australia tax rate.
        /// </summary>
        /// <remarks>
        /// This test want to confirm we always get the right value from servcie
        /// Other Dev could change the logic, but have to get same result.s
        /// </remarks>
        [Fact]
        public void GetTaxRate_Should_Return_AustraliaTaxRate()
        {
            // Arrange
            var service = new AustraliaTaxRateService();

            // Act
            var result = service.GetTaxRate();

            // Assert

            result.Should().NotBeNull();
            result.Name.Should().NotBeEmpty();
            result.Name.Should().Be("Australia Tax Rate");
            result.TaxRates.Should().NotBeEmpty();
            result.TaxRates.Count.Should().Be(5);
            result.TaxRates.Should().BeEquivalentTo(new List<TaxRateItem>
            {
                new TaxRateItem
                {
                    TaxRateMinimumSalary = 0,
                    TaxRateMaximumSalary = 20000,
                    TaxRate = 0
                },
                new TaxRateItem
                {
                    TaxRateMinimumSalary = 20001,
                    TaxRateMaximumSalary = 40000,
                    TaxRate = 0.1m
                },
                new TaxRateItem
                {
                    TaxRateMinimumSalary = 40001,
                    TaxRateMaximumSalary = 80000,
                    TaxRate = 0.2m
                },
                new TaxRateItem
                {
                    TaxRateMinimumSalary = 80001,
                    TaxRateMaximumSalary = 180000,
                    TaxRate = 0.3m
                },
                new TaxRateItem
                {
                    TaxRateMinimumSalary = 180001,
                    TaxRateMaximumSalary = decimal.MaxValue,
                    TaxRate = 0.4m
                },
            });
        }
    }
}
