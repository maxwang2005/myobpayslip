﻿using Infrastructure.TaxRate;
using Infrastructure.UnitTests.Entities;
using Infrastructure.UnitTests.TestCases;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.UnitTests.TaxRate
{
    public class AustraliaAnnualTaxCalculatorTests
    {
        [Theory]
        [ClassData(typeof(AustraliaAnnualTaxCalculatorTestCases))]
        public async Task AustraliaAnnualTaxCalculator_Should_Work(TaxCalculatorTestCase data)
        {
            // Arrange
            var calculator = new AustraliaAnnualTaxCalculator(new AustraliaTaxRateService());

            // Act
            var result = await calculator.CalculateAnnualTax(data.Input);

            // Assert
            Assert.Equal(data.Expected, result);
        }
    }
}
