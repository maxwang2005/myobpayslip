﻿using Infrastructure.UnitTests.Entities;
using System.Collections;
using System.Collections.Generic;

namespace Infrastructure.UnitTests.TestCases
{
    /// <summary>
    ///  These test cases are Australia tax culator, depend on Australia Tax Rate service
    /// </summary>
    public class AustraliaAnnualTaxCalculatorTestCases : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return GetAustraliaAnnualTaxCalculator0TestCase();
            yield return GetAustraliaAnnualTaxCalculator15000TestCase();
            yield return GetAustraliaAnnualTaxCalculator20000TestCase();
            yield return GetAustraliaAnnualTaxCalculator20001TestCase();
            yield return GetAustraliaAnnualTaxCalculator30000TestCase();
            yield return GetAustraliaAnnualTaxCalculator40000TestCase();
            yield return GetAustraliaAnnualTaxCalculator40001TestCase();
            yield return GetAustraliaAnnualTaxCalculator60000TestCase();
            yield return GetAustraliaAnnualTaxCalculator80000TestCase();
            yield return GetAustraliaAnnualTaxCalculator80001TestCase();
            yield return GetAustraliaAnnualTaxCalculator130000TestCase();
            yield return GetAustraliaAnnualTaxCalculator180000TestCase();
            yield return GetAustraliaAnnualTaxCalculator180001TestCase();
            yield return GetAustraliaAnnualTaxCalculator200000TestCase();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private object[] GetAustraliaAnnualTaxCalculator0TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 0,
                    Input = 0,
                    TestName = "Salary 0, tax should be 0"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator130000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 25000,
                    Input = 130000,
                    TestName = "Salary 130000, tax should be 25000"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator15000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 0,
                    Input = 15000,
                    TestName = "Salary 15000, tax should be 0"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator180000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 40000,
                    Input = 180000,
                    TestName = "Salary 180000, tax should be 40000"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator180001TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 40000.4m,
                    Input = 180001,
                    TestName = "Salary 180001, tax should be 40000.4"
                }
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator200000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 48000,
                    Input = 200000,
                    TestName = "Salary 200000, tax should be 48000"
                }
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator20000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 0,
                    Input = 20000,
                    TestName = "Salary 20000, tax should be 0"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator20001TestCase()
        {
            return new object[]
           {
                new TaxCalculatorTestCase
                {
                    Expected = 0.1m,
                    Input = 20001,
                    TestName = "Salary 20001, tax should be 0.1"
                },
           };
        }

        private object[] GetAustraliaAnnualTaxCalculator30000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 1000,
                    Input = 30000,
                    TestName = "Salary 30000, tax should be 1000"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator40000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 2000,
                    Input = 40000,
                    TestName = "Salary 40000, tax should be 2000"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator40001TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 2000.2m,
                    Input = 40001,
                    TestName = "Salary 40001, tax should be 2000.2"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator60000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 6000,
                    Input = 60000,
                    TestName = "Salary 60000, tax should be 6000"
                },
          };
        }

        private object[] GetAustraliaAnnualTaxCalculator80000TestCase()
        {
            return new object[]
            {
                new TaxCalculatorTestCase
                {
                    Expected = 10000,
                    Input = 80000,
                    TestName = "Salary 80000, tax should be 10000"
                },
            };
        }

        private object[] GetAustraliaAnnualTaxCalculator80001TestCase()
        {
            return new object[]
             {
                new TaxCalculatorTestCase
                {
                    Expected = 10000.3m,
                    Input = 80001,
                    TestName = "Salary 80000, tax should be 10000"
                },
             };
        }
    }
}