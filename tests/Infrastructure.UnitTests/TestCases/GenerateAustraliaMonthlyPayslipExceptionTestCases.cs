﻿using Infrastructure.UnitTests.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.UnitTests.TestCases
{
    /// <summary>
    ///  This list most possible reason when  GenerateAustraliaMonthlyPayslip will throw argument exception
    /// </summary>
    public class GenerateAustraliaMonthlyPayslipExceptionTestCases : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return GetPayslipNameNullTestCase();
            yield return GetPayslipNameEmptyTestCase();
            yield return GetPayslipNameSpaceTestCase();
            yield return GetPayslipNameSalaryNegativeTestCase();
           
        }

        private object[] GetPayslipNameSalaryNegativeTestCase()
        {
            return new object[]
            {
                new GenerateAustraliaMonthlyPayslipExceptionTestCase
                {
                    Expected = false,
                    Input = new GenerateAustraliaMonthlyPayslipInput
                    {
                        Name = "test",
                        AnnualSalary = -1
                    },
                    TestName = "Name could not be space"
                },
            };
        }

        private object[] GetPayslipNameSpaceTestCase()
        {
            return new object[]
            {
                new GenerateAustraliaMonthlyPayslipExceptionTestCase
                {
                    Expected = false,
                    Input = new GenerateAustraliaMonthlyPayslipInput
                    {
                        Name = " ",
                        AnnualSalary = 1
                    },
                    TestName = "Name could not be space"
                },
            };
        }

        private object[] GetPayslipNameEmptyTestCase()
        {
            return new object[]
            {
                new GenerateAustraliaMonthlyPayslipExceptionTestCase
                {
                    Expected = false,
                    Input = new GenerateAustraliaMonthlyPayslipInput
                    {
                        Name = null,
                        AnnualSalary = 1
                    },
                    TestName = "Name could not be null"
                },
            };
        }

        private object[] GetPayslipNameNullTestCase()
        {
            return new object[]
            {
                new GenerateAustraliaMonthlyPayslipExceptionTestCase
                {
                    Expected = false,
                    Input = new GenerateAustraliaMonthlyPayslipInput 
                    {
                        Name = string.Empty,
                        AnnualSalary = 1
                    },
                    TestName = "Name could not be empty"
                },
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
