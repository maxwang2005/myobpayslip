﻿using Application.Interfaces;
using Domain.Entities;
using Infrastructure.Paymentslip;
using Infrastructure.TaxRate;
using Infrastructure.UnitTests.Entities;
using Infrastructure.UnitTests.TestCases;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.UnitTests.Paymentslip
{
    public class GenerateAustraliaMonthlyPayslipTests
    {
        [Theory]
        [ClassData(typeof(GenerateAustraliaMonthlyPayslipExceptionTestCases))]
        public async Task GenerateAustraliaMonthlyPayslip_Should_Throw_Exception_When_Input_NotValid(GenerateAustraliaMonthlyPayslipExceptionTestCase data)
        {
            // Arrange
            var calculator = new GenerateAustraliaMonthlyPayslip(
                Mock.Of<ITaxCalculator>(),
                Mock.Of<IMonthlyPayslipOutputFormatter>());

            // Act && Assert
            await Assert.ThrowsAsync<ArgumentException>(() => calculator.GenerateMonthlyPayslip(data.Input.Name, data.Input.AnnualSalary));
            
        }

        [Fact]
        public async Task GenerateAustraliaMonthlyPayslip_Should_Call_Dependency_Services()
        {
            // Arrange
            var taxCalculator = new Mock<ITaxCalculator>();
            var formatter = new Mock<IMonthlyPayslipOutputFormatter>();
            var calculator = new GenerateAustraliaMonthlyPayslip(taxCalculator.Object, formatter.Object);

            // Act
            var result = await calculator.GenerateMonthlyPayslip("test", 60000);
            taxCalculator.Verify(m => m.CalculateAnnualTax(It.IsAny<decimal>()), Times.Once);
            formatter.Verify(m => m.PayslipFormatter(It.IsAny<MonthlyPayslip>()), Times.Once);
        }
    }
}
