﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.UnitTests.Entities
{
    public abstract class BaseTestCase<TInput, TExpected>
    {
        public string TestName { get; set; }
        public TExpected Expected { get; set; }
        public TInput Input { get; set; }

        public override string ToString()
        {
            return TestName;
        }
    }
}
