﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.UnitTests.Entities
{
    public class GenerateAustraliaMonthlyPayslipInput
    {
        public string Name { get; set; }
        public decimal AnnualSalary { get; set; }
    }
    public class GenerateAustraliaMonthlyPayslipExceptionTestCase :  BaseTestCase<GenerateAustraliaMonthlyPayslipInput, bool>
    {

    }
}
