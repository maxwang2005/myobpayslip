﻿using Application.Employee.Commands.CreateEmployeeMonthlyPayslip;
using Application.UnitTests.Entities;
using Application.UnitTests.TestCases;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Application.UnitTests.Employee.Commands.CreateEmployeeMonthlyPayslip
{
    public class CreateEmployeeMonthlyPayslipCommandValidatorTests
    {
        [Theory]
        [ClassData(typeof(CreateEmployeeMonthlyPayslipCommandValidatorTestCases))]
        public void CreateProductCommandValidator_Should_Work(CreateEmployeeMonthlyPayslipCommandValidatorTestCase data)
        {
            // Arrange
            var validator = new CreateEmployeeMonthlyPayslipCommandValidator();

            // Act
            var result = validator.Validate(data.Input);

            // Assert
            Assert.False(result.IsValid);
            Assert.Contains(result.Errors, x => x.PropertyName == data.Expected);
        }
    }
}
