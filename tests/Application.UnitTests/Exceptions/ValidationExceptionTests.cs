﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Application.Exceptions;
using FluentValidation.Results;

namespace Application.UnitTests.Exceptions
{
    public class ValidationExceptionTests
    {
        [Fact]
        public void DefaultConstructor_Should_Create_AnEmptyErrorDictionary()
        {
            // Arrange
            var exception = new ValidationException();

            // Act
            var actual = exception.Errors;

            // Assert
            Assert.Empty(actual);
        }

        [Fact]
        public void SingleValidationFailure_Should_Create_SingleErrorDictionaryItem()
        {
            // Arrange
            var failures = new List<ValidationFailure>
            {
                new ValidationFailure("Age", "must be over 18"),
            };

            var exception = new ValidationException(failures);

            // Act
            var actual = exception.Errors;

            // Assert
            Assert.NotEmpty(actual);
            Assert.Single(actual);
        }

        [Fact]
        public void MultipleValidationFailure_Should_Create_MultipleErrorDictionaryItem()
        {
            // Arrange
            var failures = new List<ValidationFailure>
            {
                new ValidationFailure("Age", "must be over 18"),
                new ValidationFailure("Password", "must contain at least 8 characters"),
            };

            var exception = new ValidationException(failures);

            // Act
            var actual = exception.Errors;

            // Assert
            Assert.NotEmpty(actual);
            Assert.Equal(2, actual.Count);
        }
    }
}
