﻿using Application.Employee.Commands.CreateEmployeeMonthlyPayslip;
using Application.UnitTests.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Application.UnitTests.TestCases
{
    public class CreateEmployeeMonthlyPayslipCommandValidatorTestCases : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return GetCreateEmployeeMonthlyPayslipCommandNameEmptyTestCase();
            yield return GetCreateEmployeeMonthlyPayslipCommandNameSpaceTestCase();
            yield return GetCreateEmployeeMonthlyPayslipCommandNameTabTestCase();
            yield return GetCreateEmployeeMonthlyPayslipCommandNameNewlineTestCase();
            yield return GetCreateEmployeeMonthlyPayslipCommandNameMixedWhiteSpaceTestCase();
            yield return GetCreateEmployeeMonthlyPayslipCommandSalary0TestCase();
            yield return GetCreateEmployeeMonthlyPayslipCommandSalaryLessThan0TestCase();
        }

        private object[] GetCreateEmployeeMonthlyPayslipCommandSalaryLessThan0TestCase()
        {
            return new object[]
            {
                new CreateEmployeeMonthlyPayslipCommandValidatorTestCase
                {
                    Expected = "AnnualSalary",
                    Input = new CreateEmployeeMonthlyPayslipCommand
                    {
                        Name = "test",
                        AnnualSalary = -1
                    },
                    TestName = "Annual Salary should not be negative"
                }
            };
        }

        private object[] GetCreateEmployeeMonthlyPayslipCommandSalary0TestCase()
        {
            return new object[]
            {
                new CreateEmployeeMonthlyPayslipCommandValidatorTestCase
                {
                    Expected = "AnnualSalary",
                    Input = new CreateEmployeeMonthlyPayslipCommand
                    {
                        Name = "test",
                        AnnualSalary = 0
                    },
                    TestName = "Annual Salary should not be 0"
                }
            };
        }

        private object[] GetCreateEmployeeMonthlyPayslipCommandNameMixedWhiteSpaceTestCase()
        {
            return new object[]
            {
                new CreateEmployeeMonthlyPayslipCommandValidatorTestCase
                {
                    Expected = "Name",
                    Input = new CreateEmployeeMonthlyPayslipCommand
                    {
                        Name = "\t \t\r\n",
                        AnnualSalary = 1
                    },
                    TestName = "Name should not be new line"
                }
            };
        }

        private object[] GetCreateEmployeeMonthlyPayslipCommandNameNewlineTestCase()
        {
            return new object[]
            {
                new CreateEmployeeMonthlyPayslipCommandValidatorTestCase
                {
                    Expected = "Name",
                    Input = new CreateEmployeeMonthlyPayslipCommand
                    {
                        Name = "\r\n",
                        AnnualSalary = 1
                    },
                    TestName = "Name should not be new line"
                }
            };
        }

        private object[] GetCreateEmployeeMonthlyPayslipCommandNameTabTestCase()
        {
            return new object[]
            {
                new CreateEmployeeMonthlyPayslipCommandValidatorTestCase
                {
                    Expected = "Name",
                    Input = new CreateEmployeeMonthlyPayslipCommand
                    {
                        Name = "\t",
                        AnnualSalary = 1
                    },
                    TestName = "Name should not be tab"
                }
            };
        }

        private object[] GetCreateEmployeeMonthlyPayslipCommandNameSpaceTestCase()
        {
            return new object[]
            {
                new CreateEmployeeMonthlyPayslipCommandValidatorTestCase
                {
                    Expected = "Name",
                    Input = new CreateEmployeeMonthlyPayslipCommand
                    {
                        Name = " ",
                        AnnualSalary = 1
                    },
                    TestName = "Name should not be space"
                }
            };
        }

        private object[] GetCreateEmployeeMonthlyPayslipCommandNameEmptyTestCase()
        {
            return new object[]
            {
                new CreateEmployeeMonthlyPayslipCommandValidatorTestCase
                {
                   Expected = "Name",
                   Input = new CreateEmployeeMonthlyPayslipCommand
                   {
                       Name = "",
                       AnnualSalary = 1
                   },
                   TestName = "Name should not be empty string"
                }
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
